#### 作者网页：[www.jcohy.com](http://www.jcohy.com)  	
> #### PS:待开发中。。。。

>  我的学习笔记，记录学习过程中的笔记以及遇到的问题,以及我的一些经验总结。如果出现链接失效,或者想知道更多的内容等情况可以提交 Issues 提醒我修改相关内容。

> #### PS:我的学习笔记,点击可以跳转到相应分类

## [jcohy-study-java](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-java/README.md)
> #### Java基础


## [jcohy-study-thread](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-thread/README.md)
> #### Ps:线程相关


## [jcohy-study-gui](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-gui/README.md)
> #### Ps:java GUI


## [jcohy-study-web](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-web/README.md)
> #### Ps:Web相关


## [jcohy-study-designpattern](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-designpattern/README.md)
> #### Ps:本文讲解java的的设计模式，用最简单的语言，案例描述各种设计模式。


## [jcohy-study-alogrithm](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-alogrithm/README.md)
> #### Ps:经典算法，排序相关代码

## [jcohy-study-DateStructures](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-DateStructures/README.md)
> #### Ps:数据结构相关

## [jcohy-study-security](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-security/README.md)
> #### Ps:安全算法相关


## [jcohy-study-files](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-files/README.md)
> #### Ps:国家列表，正则表达式，笔记，配置文件等


## [jcohy-study-utils](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-utils/README.md)
> #### Ps:相关工具类操作


##  [jcohy-study-linux](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-linux/README.md)
> #### Ps:本文讲解Linux相关知识。


##  [jcohy-study-nginx](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-nginx/README.md)
> #### Ps:Nginx相关知识。


##  [jcohy-study-nosql](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-nosql/README.md)
> #### Ps:nosql相关知识。


##  [jcohy-study-zookeeper](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-zookeeper/README.md)
> #### Ps:zookeeper相关知识。


##  [jcohy-study-SpringBoot](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-springboot/README.md)
> #### Ps:SpringBoot相关知识。


##  [Swagger](https://github.com/jiachao23/jcohy-study-sample/blob/master/Swagger.md)
> #### Ps:Swagger相关知识。


##  [WebSite](https://github.com/jiachao23/jcohy-study-sample/blob/master/website.md)
> #### Ps:相关网站