package com.jcohy.study.lambda;

/**
 * Created by jiac on 2018/9/4.
 * ClassName  : com.jcohy.study.lambda
 * Description  :
 */
@FunctionalInterface
public interface Func1 {
    public String getValue(String str);
}
