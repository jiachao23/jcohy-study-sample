package com.jcohy.study.lambda;

/**
 * Created by jiac on 2018/9/4.
 * ClassName  : com.jcohy.study.lambda
 * Description  :
 */
public interface Func2<T,R> {

    R getValue(T t1,T t2);
}
