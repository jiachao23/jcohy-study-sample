#### 作者网页：[www.jcohy.com](http://www.jcohy.com)  	
> #### PS:待开发中。。。。

>  我的学习笔记，记录学习过程中的笔记以及遇到的问题,以及我的一些经验总结。如果出现链接失效,或者想知道更多的内容等情况可以提交 Issues 提醒我修改相关内容。

> #### PS:我的学习笔记,点击可以跳转到相应分类
>  Linux的简介和安装这里不做说明，网上的教程一大把。
# [Linux]
 #### Ps:本目录讲解Linux相关知识。
 * [Linux的镜像地址](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-linux/markdown/Mirrors.md)
 * [Linux的文件目录结构](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-linux/markdown/LinuxDir.md)
 * [Linux的文件常用命令](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-linux/markdown/Command.md)
 * [Linux下常用软件安装](https://github.com/jiachao23/jcohy-study-sample/tree/master/jcohy-study-linux/markdown/install.md)